#!/usr/bin/env python3
import argparse
import heapq
import ntpath
import numpy as np
from collections import defaultdict
from collections import Counter
from queue import PriorityQueue

graph = defaultdict(dict)
parent = {}
best = {}
width = height = 0
starts = []
ends = []
time = defaultdict(list)
paths = defaultdict(list)
""""
Procedure returns tail from path (for example: file name)

Input: path
"""
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail


""""
Procedure returns head from path (for example: all but file name)

Input: path
"""
def path_head(path):
    head, tail = ntpath.split(path)
    return head


""""
Procedure gets parameters from file name - height, width from map

Input: file name
"""
def get_parameters(map_file):
    global width, height
    split = map_file.split('_')
    width, height = split[0].split('x')
    width = int(width)
    height = int(height)


""""
Procedure gets positions of starts and ends from map file

Input: loaded file from text file
"""
def get_starts_ends(lines):
    global width, height, starts, ends
    tiles = lines[1:width * height + 1]
    for tile in tiles:
        parsed_tile = tile.split(',')
        if int(parsed_tile[1]) != 0:
            starts.append((int(parsed_tile[1]), int(parsed_tile[0][1:])))
        if int(parsed_tile[2][:-2]) != 0:
            ends.append((int(parsed_tile[2][:-2]), int(parsed_tile[0][1:])))
    starts.sort(key=lambda tup: tup[0])
    ends.sort(key=lambda tup: tup[0])


""""
Procedure creates graph from map file

Input: loaded file from text file
"""
def add(lines):
    global width, height, graph, best
    edges = lines[width * height + 2:]
    for edge in edges:
        parsed_edge = edge.split(',')
        first = int(parsed_edge[0][1:])
        second = int(parsed_edge[1][:-2])
        # Constant value of cost every edge (All edges are with same cost)
        value = 52
        # {1,2} - 1->2
        graph[first][second] = value
        # {1,2} - 2->1
        graph[second][first] = value
        best[first] = np.inf
        best[second] = np.inf


""""
Procedure saves the map to a file for possible checking

Input: path to file, file name, loaded file from text file
"""
def save_map(map_file, file_name, lines):
    global graph
    unique_edge = set()
    f = open(map_file + "/save/" + file_name, "w")
    for line in lines[:width * height + 2]:
        f.write(line)
    for key in graph:
        for second_key in graph[key].keys():
            if (abs(key - second_key) == 1) or (abs(key - second_key) == width):
                if second_key > key:
                    unique_edge.add((key, second_key))
                else:
                    unique_edge.add((second_key, key))
    for edge in unique_edge:
        f.write('{' + format(edge[0]) + ',' + format(edge[1]) + '}\n')
    f.close()


""""
Procedure loads graph, starts positions, end positions, and all dimensions of map

Input: full path to file with file name
"""
def load_map(map_file):
    f = open(map_file, "r")
    lines = f.readlines()
    file_name = path_tail(map_file)
    path = path_head(path_head(map_file))
    get_parameters(file_name)
    get_starts_ends(lines)
    add(lines)
    save_map(path, file_name, lines)
    f.close()
    return path


""""
Procedure gets whole path from start to end

Input: no of agent, start node, end node
"""
def print_path(no, start, node):
    global parent, paths
    if node == start:
        paths[no].append(node)
        return
    print_path(no, start, parent[node])
    paths[no].append(node)


""""
Procedure binds discrete time on found paths 

Input: no of agent
"""
def set_times(no):
    global time, paths
    counter = 0
    for step in range(len(paths[no]) - 1):
        time[counter].append((no, paths[no][step], paths[no][step + 1]))
        counter += 1


""""
Procedure to clear variables
"""
def reset():
    global best, parent
    for v in best:
        best[v] = np.inf
    parent.clear()


""""
Procedure - Dijkstra algorithm - with heap - for finding shortest path from start vertex

Input: start vertex
"""
def dijkstra(start):
    global graph, best
    visited = set()
    best[start] = 0
    unvisited_queue = [(best[v], v) for v in best]
    heapq.heapify(unvisited_queue)
    while len(unvisited_queue):
        distance, vertex = heapq.heappop(unvisited_queue)
        visited.add(vertex)
        for neigh in graph[vertex]:
            if neigh in visited:
                continue
            new_dist = distance + graph[vertex][neigh]
            if new_dist < best[neigh]:
                best[neigh] = new_dist
                parent[neigh] = vertex
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)
        unvisited_queue = [(best[v], v) for v in best if v not in visited]
        heapq.heapify(unvisited_queue)


""""
Procedure gets number of steps in the longest plan of agent (so we can add stops actions to other agents)
"""
def n_wait():
    global time
    counter = 0
    for t in time:
        for move in time[t]:
            if move[1] == move[2]:
                counter += 1
    return counter + len(time)


""""
Procedure prints whole plan into file and into console.

Input: path of input map (without file name)
"""
def print_output(path):
    global time, starts
    waits = n_wait()
    last_move = ""
    output_text = "================================================================\n" \
                  "Multi-Agent Path Finding (MAPF-R) Solver\n" \
                  "2022 Tomas Holas" \
                  "\n================================================================\n"
    f = open(path + "/out/output.txt", "w")
    # printing positions of each agent in time
    for agent in range(len(starts)):
        moves = 1
        output_text += "Agent " + format(agent + 1) + ": " + format(starts[agent][1])
        for t in time:
            for move in time[t]:
                if move[0] == agent + 1:
                    output_text += " " + format(move[2])
                    last_move = move
                    moves += 1
        if moves != waits:
            to_add = waits - moves
            for a in range(to_add):
                output_text += " " + format(last_move[2])
        output_text += "\n"
    for t in time:
        output_text += "    Step " + format(t) + ": "
        for move in time[t]:
            # not inserting waiting (2->2)
            if move[1] != move[2]:
                output_text += format(move[0]) + "#" + format(move[1]) + "->" + format(move[2]) + " "
        output_text += "\n"
    print(output_text)
    f.write(output_text)
    f.close()


""""
Procedure move steps in time by one

Input: time, agent, move to be moved
"""
def move_steps(t, agent, move):
    global time, paths
    # end recursion
    if t > len(time):
        return
    # need to add one more step (6 steps, needs to add 7th)
    if t == len(time):
        time[t].append(move)
        return
    time[t].append(move)
    for x in time[t]:
        if x[0] == agent:
            move = x
            break
    time[t].remove(move)
    time[t].sort(key=lambda tup: tup[0])
    move_steps(t + 1, agent, move)


""""
Procedure finds moves, that has to_vertex same and returns theirs indexes [(agent, from_vertex, to_vertex), ...]

Input: array of indexes of moves with same to_vertex, if there is 0 duplicities returns []
"""
def find_duplicities(moves):
    duplicities = defaultdict(list)
    for i, x in enumerate(moves):
        duplicities[x[2]].append(i)
    for d in duplicities:
        # some moves have same to_vertex
        if len(duplicities[d]) > 1:
            return duplicities[d]
    return []


""""
Procedure solves all collisions across all plans (all agents)
"""
def check_collisions():
    global time
    collisions = True
    while collisions:
        collisions = False
        for t in time.copy():
            duplicities = find_duplicities(time[t])
            if duplicities:
                move_steps(t, time[t][duplicities[1]][0], (time[t][duplicities[1]][0], time[t][duplicities[1]][1], time[t][duplicities[1]][1]))
                collisions = True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # full path to map with file name
    parser.add_argument('--input-file', type=str, required=True)
    # not needed, only for Vizualizer from J.CHudý  to work
    parser.add_argument('--algorithm', type=str, required=False)
    args = parser.parse_args()
    path = load_map(args.input_file)
    for start_from, end_to in zip(starts, ends):
        dijkstra(start_from[1])
        print_path(start_from[0], start_from[1], end_to[1])
        set_times(start_from[0])
        reset()
    check_collisions()
    print_output(path)
